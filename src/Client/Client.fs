module Client

open Elmish
open Elmish.React

open Fable.Core.JsInterop
open Fable.React
open Fable.React.Props
open Shared
open Fulma
open Fable.FontAwesome
open Fable.FontAwesome.Free
open Fulma.Extensions.Wikiki
open ReactEditor
open Thoth.Json

open Config
open FSharp.Reflection

importSideEffects "./sass/style.sass"
module Result =
    let toOption = function
        | Ok x -> Some x
        | Error _ -> None

module URI =
    type QueryDataType = Encoded | Plain

    let private compressToEncodedURIComponent(_x: string): string  = importMember "./js/util.js"
    let private decompressFromEncodedURIComponent(_x: string): string  = importMember "./js/util.js"
    let private getURIhash(): string  = importMember "./js/util.js"
    let private setURIhash(_x: string): unit  = importMember "./js/util.js"

    let parseQuery queryTypes =
        getURIhash().Split[|'&'|]
        |> Seq.choose (fun s ->
            match s.Split[|'='|] |> Seq.toList with
            | [key; value] ->
                let typ = queryTypes |> Map.tryFind key |> Option.defaultValue Encoded
                Some (key, match typ with Encoded -> decompressFromEncodedURIComponent value | Plain -> value)
            | _ -> None)
        |> Map.ofSeq

    let getQuery queryTypes xs =
        xs |> Seq.map (fun (key, value) ->
            let typ = queryTypes |> Map.tryFind key |> Option.defaultValue Encoded
            key + "=" + match typ with Encoded -> compressToEncodedURIComponent value | Plain -> value)
         |> String.concat "&"
    let updateQuery queryTypes xs =
        getQuery queryTypes xs |> setURIhash

// The Msg type defines what events/actions can occur while the application is running
// the state of the application changes *only* in reaction to these events
type Msg =
| VersionFound of string
| OptionsFound of FantomasOptions
| SetSourceText of string
| DoFormat of FantomasOptions
| Formatted of string
| Error of string
| UpdateConfig of FantomasOptions
| ChangeBackend of string
| SetFileName of string

module Server =

    open Shared
    open Fable.Remoting.Client

    /// A proxy you can use to talk to server directly
    let api backendName : IModelApi =
      let backendUrl = Config.getBackendUrl backendName
      Remoting.createApi()
      |> Remoting.withRouteBuilder Route.builder
      #if !DEBUG
      |> Remoting.withBaseUrl backendUrl
      #endif
      |> Remoting.buildProxy<IModelApi>

let reInitCmd model =
    let versionCmd = Cmd.OfAsync.either (Server.api model.BackendName).version () VersionFound (fun ex -> Error ex.Message)
    let optionsCmd = Cmd.OfAsync.either (Server.api model.BackendName).options () OptionsFound (fun ex -> Error ex.Message)
    Cmd.batch [versionCmd; optionsCmd; Cmd.ofMsg (DoFormat model.GetConfig)]

let queryTypes = Map.ofSeq ["fantomas", URI.Plain; "code", URI.Encoded; "config", URI.Encoded]

// defines the initial state and initial command (= side-effect) of the application
let init () : Model * Cmd<Msg> =
    let initialModel = Model.Default
    let query = URI.parseQuery queryTypes
    let code = query |> Map.tryFind "code" |> Option.defaultValue ""
    let filename = query |> Map.tryFind "filename" |> Option.defaultValue initialModel.FileName
    let config =
        query |> Map.tryFind "config" |> Option.bind (FantomasOptions.ofJson)
        |> fun x -> printfn "%A" x; x
        |> Option.defaultValue initialModel.Config
    let backendName = query |> Map.tryFind "fantomas" |> Option.defaultValue initialModel.BackendName |> azureFunctionsApplyRedirect
    let m = { initialModel with Source = code; Config = config; BackendName = backendName; FileName = filename }
    m, reInitCmd m

let updateQuery model =
    let queryData =
        [ ("fantomas", (fun m -> m.BackendName))
          ("code", (fun m -> m.Source))
          ("config", (fun m -> FantomasOptions.toJson m.Config))
          ("filename", (fun m -> m.FileName)) ]
        |> List.filter (fun ( _, selector) -> selector model <> selector Model.Default)
        |> List.map (fun (k, selector) -> k, selector model)
    URI.updateQuery queryTypes queryData

// The update function computes the next state of the application based on the current state and the incoming events/messages
// It can also run side-effects (encoded as commands) like calling the server via Http.
// these commands in turn, can dispatch messages to which the update function will react.
let update (msg : Msg) (currentModel : Model) : Model * Cmd<Msg> =
    match msg with
    | SetSourceText x ->
        let nextModel = { currentModel with Source = x; FSharpEditorState = Loaded "" }
        nextModel, Cmd.none
    | Formatted x ->
        let nextModel = { currentModel with FSharpEditorState = Loaded x }
        nextModel, Cmd.none
    | Error x ->
        let nextModel = { currentModel with FSharpEditorState = FormatError x }
        nextModel, Cmd.none
    | DoFormat config ->
        updateQuery currentModel
        let response =
            Cmd.OfAsync.either
                (fun x -> (Server.api currentModel.BackendName).format currentModel.FileName x config)
                currentModel.Source
                (function | Ok x -> Formatted x | Result.Error e -> Error e)
                (fun e -> Error e.Message)
        { currentModel with FSharpEditorState = Loading }, response
    | UpdateConfig c -> { currentModel with Config = Model.configDiffFromDefault c currentModel.DefaultConfig }, Cmd.none

    | VersionFound version -> { currentModel with Version = version }, Cmd.none
    | OptionsFound defaultConfig ->
        let newConfig = Model.configDiffFromDefault currentModel.Config defaultConfig
        { currentModel with Config = newConfig; DefaultConfig = defaultConfig }, Cmd.none
    | ChangeBackend name ->
        let m = { currentModel with BackendName = name; Version = Model.Default.Version }
        updateQuery m
        m, reInitCmd m
    | SetFileName fileName ->
        { currentModel with FileName = fileName }, Cmd.none


let options (config: FantomasOptions) dispatch =
    let update fieldName x = config |> Map.ofArray |> Map.add fieldName x |> Map.toArray

    let wrapColumn x =
        Column.column [Column.Width(Screen.All, Column.IsHalf)] (List.map snd x)

    config
    |> Seq.sortBy (fun (k, v) -> (match v with | IntOption _ -> 0 | BoolOption _ -> 1), k)
    |> Seq.map (fun (k,v) ->
        match v with
        | IntOption v ->
            Field.div [ ] [ Label.label [ ] [ str k ]; Control.div [] [ Input.number [
                Input.Option.Value (string v); Input.Option.OnChange (fun ev -> dispatch (UpdateConfig <| update k (IntOption !!ev.target?value)))] ] ]
        | BoolOption v ->
            Field.div [ ] [ Control.div [] [ Checkbox.checkbox [ ]  [
                Switch.switch [ Switch.Id k; Switch.Checked v; Switch.OnChange (fun ev -> dispatch (UpdateConfig <| update k (BoolOption !!ev.target?``checked``)))  ] [ str k ] ] ] ]
    )
    |> Seq.toList
    |> List.mapi (fun idx t -> idx,t)
    |> List.partition (fun (idx,_) -> idx % 2 = 0)
    |> fun (g1,g2) ->
        Columns.columns [Columns.CustomClass "options"] [ wrapColumn g1 ; wrapColumn g2 ]

let githubIssueUri (m : Model) =
    let location = Browser.Dom.window.location
    let config = m.GetConfig |> Seq.sortBy fst
    let defaultValues = m.DefaultConfig  |> Seq.sortBy fst |> Seq.map snd
    let options = Seq.zip config defaultValues
               |> Seq.toArray
               |> Seq.map (fun ((k,v), defV) ->
                    sprintf (if v<>defV then "| **`%s`** | **`%s`** |" else "| `%s` | `%s` |") k (FantomasOption.toString v))
               |> String.concat "\n"
    let title = "Bug report from fantomas-ui"
    let label = "bug"
    let codeTemplate header code =
        sprintf """
#### %s

```fsharp
%s
```
            """ header code
    let (left,right) =
        match m.FSharpEditorState with
        | Loading -> codeTemplate "Code" m.Source, ""
        | FormatError e ->
            codeTemplate "Code" m.Source, codeTemplate "Error" e
        | Loaded code ->
            codeTemplate "Code" m.Source, codeTemplate "Result" code
    let code = left + "" + right
    let body =
        sprintf """
Issue created from [fantomas-ui](%s)

Please describe here fantomas problem you encountered
%s
%s
#### Options

Fantomas %s

| Name | Value |
| ---- | ----- |
%s
        """ location.href left right m.Version options |> System.Uri.EscapeDataString

    let uri = sprintf "https://github.com/fsprojects/fantomas/issues/new?title=%s&labels=%s&body=%s" title label body
    uri
    |> Href

let safeComponents model =
    let fantomasUIlinks =
        span [ ]
           [
             a [ Href "https://gitlab.com/jindraivanek/fantomas-ui" ] [ str "source code" ]
             str ", "
             a [ Href "https://gitlab.com/jindraivanek/fantomas-ui/issues/new" ] [ str "create issue" ]
           ]

    let fantomasLinks model =
        span [ ]
           [
             a [ Href "https://github.com/fsprojects/fantomas" ] [ str "source code" ]
             str ", "
             a [ githubIssueUri model ] [ str "create issue" ]
           ]

    [ p [ ]
        [ strong [] [ str "Fantomas-UI" ]
          str "(this page): "
          fantomasUIlinks ]
      p [ ]
        [ strong [] [ str "Fantomas: " ]
          fantomasLinks model ] ]

let button disabled txt onClick =
    Button.button
        [ Button.IsFullWidth
          Button.Color IsPrimary
          Button.OnClick onClick
          Button.Disabled disabled ]
        [ str txt ]

let private navbarVersion isActive key txt onClick =
    Navbar.Item.a [ Navbar.Item.IsActive isActive
                    Navbar.Item.Props [ OnClick onClick
                                        Key key ] ]
        [ str txt ]

let notify txt =
    Notification.notification [ Notification.Color IsDanger ] [str txt]

let viewNavbar model (dispatch : Msg -> unit) =
        Navbar.navbar [ ]
                [ Navbar.Brand.div [ ]
                    [ Navbar.Item.a [ ]
                        [ strong [ ]
                            [ sprintf "Fantomas v%s" model.Version |> str ] ]  ]

                  Navbar.Start.div [ ]
                    [ Navbar.Item.div [ Navbar.Item.HasDropdown
                                        Navbar.Item.IsHoverable ]
                        [ Navbar.Link.div [ ]
                            [ str "Choose your version" ]

                          Navbar.Dropdown.div [ ]
                            [ Config.azureFunctions
                                |> List.map (fun x ->
                                    if x.Name <> model.BackendName then
                                        navbarVersion false x.Name x.DisplayName (fun ev -> dispatch (ChangeBackend x.Name))
                                    else
                                        navbarVersion true x.Name x.DisplayName ignore
                                )
                                |> ofList ] ] ]

                  Navbar.End.div [ ]
                    [ Navbar.Item.div [ ]
                        [ Button.a [ Button.Props [ Href "https://gitlab.com/jindraivanek/fantomas-ui" ]
                                     Button.Color IsWarning ]
                            [ Icon.icon [ ]
                                [ Fa.i [Fa.Brand.Gitlab] [] ]
                              span [ ]
                                [ str "Gitlab" ] ] ] ] ]

let sourceAndFormatted model dispatch =
    let sourceTooBig = model.Source.Length > Const.sourceSizeLimit
    let sourceIsEmpty = model.Source.Trim().Length = 0
    let btnFormatDisabled = sourceTooBig || sourceIsEmpty || model.IsLoading
    let headers =
        Columns.columns [ Columns.IsGapless ; Columns.IsMultiline ; Columns.CustomClass "is-gapless" ]
                        [ Column.column [ Column.Width(Screen.All, Column.IsHalf) ]
                            [ Message.message [ Message.Props [Id "input-message"] ]
                                [ Message.body [ ]
                                    [ Text.div [ Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ]
                                        [ str "Type or paste F# code" ] ] ] ]
                          Column.column [ Column.Width(Screen.All, Column.IsHalf) ]
                            [ Message.message [ Message.Props [Id "formatted-message"] ]
                                [ Message.body [ ]
                                    [ Text.div [ Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ]
                                        [ str "F# code formatted with Fantomas" ] ] ] ] ]

    let editors =
        Columns.columns [ Columns.IsGapless ; Columns.IsMultiline ; Columns.CustomClass "is-gapless" ]
            [
              Column.column [] [
                Editor.editor [ Editor.Language "fsharp"
                                Editor.IsReadOnly false
                                Editor.Value model.Source
                                Editor.OnChange (SetSourceText >> dispatch) ]
                (if sourceTooBig then notify "Source code size is limited to 100 kB." else div [] [])
                ]
              Column.column [] [
                  Control.div [Control.IsLoading model.IsLoading; Control.CustomClass "is-large"] [
                      Editor.editor [ Editor.Language "fsharp"
                                      Editor.IsReadOnly true
                                      Editor.Value model.Formatted ]
                      (if model.IsLoading || not model.IsError then div [] []
                       else Button.a
                                [ Button.IsFullWidth
                                  Button.Color IsPrimary
                                  Button.IsOutlined
                                  Button.Disabled btnFormatDisabled
                                  Button.Props [githubIssueUri model]
                                ] [str "Looks wrong? Create an issue!"])
            ] ] ]

    [ headers ; editors; button btnFormatDisabled (if model.IsLoading then "Formatting..." else "Format") (fun ev -> dispatch (DoFormat model.GetConfig)) ]

let footer model =
    Footer.footer [ ]
                  [ Content.content [ Content.Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ] (safeComponents model) ]

let fileName model dispatch =
    Columns.columns [ Columns.CustomClass "options" ]
        [ Column.column [ Column.CustomClass "is-one-third" ]
              [ Field.div []
                    [ Label.label [] [ str "File name:" ]
                      Control.div []
                          [ Input.text
                              [ Input.Option.Value(model.FileName)
                                Input.Option.OnChange(fun ev ->
                                    ev.Value
                                    |> SetFileName
                                    |> dispatch) ] ] ] ]
          Column.column [] [] ]


let view (model : Model) (dispatch : Msg -> unit) =
    div []
        [ viewNavbar model dispatch
          div [ Class "page-content"]
                  [
                    yield! sourceAndFormatted model dispatch
                    yield Heading.h3 [ Heading.Props [Id "config-title" ] ] [ str "Config" ]
                    yield fileName model dispatch
                    yield options (model.GetConfig) dispatch
                    yield footer model
                  ]

        ]

#if DEBUG
open Elmish.Debug
open Elmish.HMR
#endif

Program.mkProgram init update view
#if DEBUG
|> Program.withConsoleTrace
#endif
|> Program.withReactBatched "elmish-app"
#if DEBUG
|> Program.withDebugger
#endif
|> Program.run
