module Server
open System.IO
open System.Threading.Tasks

open Microsoft.AspNetCore.Http
open FSharp.Control.Tasks.V2
open Giraffe
open Shared
open Microsoft.Azure.WebJobs
open Microsoft.Azure.WebJobs.Extensions.Http
open Microsoft.Extensions.Logging

open Fable.Remoting.Server
open Fable.Remoting.Giraffe
open Fantomas
open Fable.SimpleJson

let publicPath = Path.GetFullPath "../Client/public"
let port = 8085us

module Result =
    let attempt f =
        try
            Ok <| f()
        with e -> Error e

module Reflection =
    open FSharp.Reflection
    let optionsToConfig<'t> (x:FantomasOptions) (def: 't) =
        let oldValues = x |> Map.ofArray |> Map.map (fun _ v -> match v with | IntOption x -> box x | BoolOption x -> box x)
        let values = Reflection.getRecordFields def |> Seq.map (fun (k, v) -> Map.tryFind k oldValues |> Option.defaultValue v) |> Seq.toArray
        FSharpValue.MakeRecord(typeof<'t>, values) :?> 't

let getOptions() = async {
    let options =
        Reflection.getRecordFields Fantomas.FormatConfig.FormatConfig.Default
        |> Seq.map (fun (key, x) -> match x with | :? int as x -> key, IntOption x | :? bool as x -> key, BoolOption x)
        |> Array.ofSeq
    return options
    }

let lck = obj()

let getFantomasVersion () =
    let assembly = typeof<Fantomas.FormatConfig.FormatConfig>.Assembly
    let version = assembly.GetName().Version
    let date = System.IO.FileInfo assembly.Location |> fun f -> f.LastWriteTime.ToShortDateString()
    sprintf "%i.%i.%i" version.Major version.Minor version.Build
    |> fun v -> async { return v }

let init() : Task<Model> = task { return Model.Default }

let format filename (x: string) config =
    let checker = Fantomas.FakeHelpers.sharedChecker.Force()
    let options = Fantomas.FakeHelpers.createParsingOptionsFromFile filename
    task {
        return
            if x.Length > Const.sourceSizeLimit then Error "Source size limit exceeded." else
            lock lck (fun () ->
                Result.attempt <| fun () -> (Fantomas.CodeFormatter.ParseAsync(filename, SourceOrigin.SourceString x, options, checker) |> Async.RunSynchronously)
                |> Result.bind (fun _ ->
                    Result.attempt <| fun () ->
                        (Fantomas.CodeFormatter.FormatDocumentAsync(filename, SourceOrigin.SourceString x, Reflection.optionsToConfig<Fantomas.FormatConfig.FormatConfig> config Fantomas.FormatConfig.FormatConfig.Default, options, checker)) |> Async.RunSynchronously)
                |> Result.mapError (fun exn -> sprintf "Exception: %s Stack Trace: %s" exn.Message exn.StackTrace)
                |> Result.bind (fun code ->
                    Result.attempt (fun () -> (Fantomas.CodeFormatter.ParseAsync(filename, SourceOrigin.SourceString code, options, checker)) |> Async.RunSynchronously)
                    |> Result.map (fun _ -> code) |> Result.mapError (fun _ -> code))) }

let modelApi = {
    init = init >> Async.AwaitTask
    format = fun fileName x config -> format fileName x config |> Async.AwaitTask
    version = getFantomasVersion
    options = getOptions
}

let webApp =
    Remoting.createApi()
    |> Remoting.withRouteBuilder Route.builder
    |> Remoting.fromValue modelApi
    |> Remoting.buildHttpHandler

// code from https://github.com/giraffe-fsharp/Giraffe.AzureFunctions
[<FunctionName "Giraffe">]
let run ([<HttpTrigger (AuthorizationLevel.Anonymous, Route = "{*any}")>] req : HttpRequest, context : ExecutionContext, log : ILogger) =
  let hostingEnvironment = req.HttpContext.GetHostingEnvironment()
  hostingEnvironment.ContentRootPath <- context.FunctionAppDirectory
  let func = Some >> Task.FromResult
  task {
    let! _ = webApp func req.HttpContext
    req.HttpContext.Response.Body.Flush() //workaround https://github.com/giraffe-fsharp/Giraffe.AzureFunctions/issues/1
    } :> Task
