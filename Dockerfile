FROM nojaf/fable:2.5 AS builder
SHELL ["/bin/bash", "-c"]

RUN npm -g config set user root
RUN npm install -g azure-functions-core-tools@preview

WORKDIR /build

COPY .config .config
RUN dotnet tool restore

COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile
COPY paket.dependencies paket.lock ./
RUN dotnet paket restore
COPY src src
COPY build.fsx ./
RUN rm -rf deploy

# Build client files
EXPOSE 8080
ENTRYPOINT ["dotnet", "fake", "run", "build.fsx", "--target", "run"]