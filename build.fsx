#r "paket: groupref build //"
#load "./.fake/build.fsx/intellisense.fsx"

#load "src/Shared/Config.fs"

open Fake.Core
open Fake.DotNet
open Fake.IO
open Fake.IO.FileSystemOperators
open Fake.JavaScript

let serverPath = Path.getFullName "./src/Server"
let clientPath = Path.getFullName "./src/Client"
let deployDir = Path.getFullName "./deploy"
let serverDeployDir = Path.combine deployDir "Server"

let platformTool tool winTool =
    let tool = if Environment.isUnix then tool else winTool
    match ProcessUtils.tryFindFileOnPath tool with
    | Some t -> t
    | _ ->
        let errorMsg =
            tool + " was not found in path. " +
            "Please install it and make sure it's available from your path. " +
            "See https://safe-stack.github.io/docs/quickstart/#install-pre-requisites for more info"
        failwith errorMsg

let nodeTool = platformTool "node" "node.exe"
let yarnTool = platformTool "yarn" "yarn.cmd"
let funcTool() = platformTool "func" "func"

let createCommand cmd args workingDir =
    let arguments = args |> String.split ' ' |> Arguments.OfArgs
    Command.RawCommand (cmd, arguments)
    |> CreateProcess.fromCommand
    |> CreateProcess.withWorkingDirectory workingDir
    |> CreateProcess.ensureExitCode
let runTool cmd args workingDir =
    createCommand cmd args workingDir
    |> Proc.run
    |> ignore

let runDotNet cmd workingDir =
    let result =
        DotNet.exec (DotNet.Options.withWorkingDirectory workingDir) cmd ""
    if result.ExitCode <> 0 then failwithf "'dotnet %s' failed in %s" cmd workingDir

let openBrowser url =
    //https://github.com/dotnet/corefx/issues/10361
    Command.ShellCommand url
    |> CreateProcess.fromCommand
    |> CreateProcess.ensureExitCodeWithMessage "opening browser failed"
    |> Proc.run
    |> ignore

Target.create "Clean" (fun _ ->
    Shell.cleanDirs [deployDir]
)

Target.create "InstallClient" (fun _ ->
    printfn "Node version:"
    runTool nodeTool "--version" __SOURCE_DIRECTORY__
    printfn "Yarn version:"
    runTool yarnTool "--version" __SOURCE_DIRECTORY__
    runTool yarnTool "install --frozen-lockfile" __SOURCE_DIRECTORY__
    runDotNet "restore" clientPath
)

Target.create "RestoreServer" (fun _ ->
    runDotNet "restore" serverPath
)

Target.create "BuildServer" (fun _ ->
    let publishArgs = sprintf "publish -c Release -o \"%s\"" serverDeployDir
    runDotNet publishArgs serverPath
)

Target.create "BuildClient" (fun _ ->
    Yarn.exec "webpack --config src/Client/webpack.config.js -p" (fun args -> { args with WorkingDirectory = clientPath })
)

Target.create "Build" ignore

let runServer() =
    Directory.ensure serverDeployDir
    runDotNet (sprintf "publish -o %s" serverDeployDir) serverPath
    runTool (funcTool()) "start -p 8085" serverDeployDir

Target.create "Run" (fun _ ->
    let server = async {
        runServer()
    }
    let client = async {
        Yarn.exec "webpack-dev-server --config src/Client/webpack.config.js" (fun args -> { args with WorkingDirectory = clientPath })
    }
    let browser = async {
        do! Async.Sleep 5000
        openBrowser "http://localhost:8080"
    }

    [ server; client; browser ]
    |> Async.Parallel
    |> Async.RunSynchronously
    |> ignore
)

Target.create "RunServer" (fun _ ->
    runServer()
)

Target.create "Bundle" (fun _ ->
    let clientDir = Path.combine deployDir "Client"
    let publicDir = Path.combine clientDir "public"

    Shell.copyDir publicDir "src/Client/output" FileFilter.allFiles
)

open Fake.Core.TargetOperators

"Clean" ==> "InstallClient"
"Clean" ==> "RestoreServer"
"RestoreServer" ==> "BuildServer" ==> "Build"
"InstallClient" ==> "BuildClient" ==> "Build"
"InstallClient" ==> "BuildClient" ==> "Bundle"

"InstallClient" ==> "Run"
"RestoreServer" ==> "Run"

Target.runOrDefault "Build"
